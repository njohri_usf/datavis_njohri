String[] rawData = new String[7];
int[] value1 = new int[7];
int[] value2 = new int[7];
File myFile = null;
int margin, graphHeight;
float xSpacer;
PVector[] position = new PVector[10];

void setup() {
  size(600, 600);
  selectInput("Select a file to process:", "processData");
}

void draw() {
  background(20);
  fill(200);
  for(int i=0; i<position.length; i++){
   ellipse(position[i].x, position[i].y, 15,15); 
  }
}

void processData(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
  }
  myFile = selection;
  rawData = loadStrings(myFile);
  //printArray(rawData);
  for (int i =1; i<rawData.length; i++) {
    String[] thisRow = split(rawData[i], ",");
    //printArray(thisRow);
    value1[i-1] = int(thisRow[1]);
    printArray(value1);
    value2[i-1] = int(thisRow[2]);
  }

  int overallMin = min(value2);
  int overallMax = max(value2);

  margin = 50;
  graphHeight = (height-margin) - margin;
  xSpacer = (width - margin - margin) / (value1.length - 1);

  for (int i=0; i<value2.length; i++) {
    float adjValue2 = map(value1[i], overallMin, overallMax, 0, graphHeight);
    float ypos = height - margin - adjValue2; 
    float xpos = margin + (xSpacer * i) ;
    position[i] = new PVector(xpos, ypos);
    
  }
}