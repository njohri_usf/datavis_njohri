String[] rawData= null;
int[] years= null;
int[] horsePower= null;
int[] weight= null;
int[] grade= null;
File myFile= null;
int MAX_LEN = 10;

void setup() {
  size(600, 600);
  rawData = new String[MAX_LEN];
  horsePower = new int[MAX_LEN];
  selectInput("Select a file to process:", "fileSelected");

}

void draw () {
  background(255);
  fill(0);
  stroke(255);
  
  //horsePower = new int[rawData.length]; 
  ////printArray(rawData);
  //for (int i=1; i<rawData.length; i++) {
  //  String[] thisRow = split(rawData[i], ",");
  //  //printArray(thisRow);
    
  //  years[i-1] = int(thisRow[0]);
  //  horsePower[i-1]= int(thisRow[1]);
  //}
  //sprintln(horsePower);
  for (int i=0; i<horsePower.length; i++) {
    int rectWidth = width/ horsePower.length;
    int ypos = height - (horsePower[i]*2);
    rect((rectWidth*i), ypos, rectWidth, horsePower[i]*2);
  }
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
  }
  myFile = selection;
  rawData =loadStrings(myFile);
   //printArray(rawData);
  years = new int[rawData.length];
 // horsePower = new int[rawData.length]; 
  ////printArray(rawData);
  for (int i=1; i<rawData.length; i++) {
    String[] thisRow = split(rawData[i], ",");
    //printArray(thisRow);
    
    years[i-1] = int(thisRow[0]);
    horsePower[i-1]= int(thisRow[1]);
  }
  //printArray(years);
}