class BarChart extends Frame {

  float minX, maxX;
  float minY, maxY;
  int idx0, idx1;
  int border = 40;
  boolean drawLabels = true;
  float spacer = 20;
  int colCount;



  BarChart( Table data, ArrayList<Integer> useColumns ) {

    //this.idx0 = idx0;
    //this.idx1 = idx1;
    colCount = useColumns.size();
    for (int i=0; i<colCount; i++) {
      //make an array so that when we click a button the idx0 updates
    }
    idx0 = useColumns.get(0);
    println(idx0);
    idx1 = useColumns.get(1);

    minX = min(data.getFloatColumn(idx0));
    maxX = max(data.getFloatColumn(idx0));

    minY = min(data.getFloatColumn(idx1));
    maxY = max(data.getFloatColumn(idx1));

    //table.getColumnTitle();
    //table.getRowCount()
    //table.getRow()
    // row.getFloat();
  }

  void draw() {

    //same as all the draw functions 
    for ( int i = 0; i < table.getRowCount(); i++ ) {
      TableRow r = table.getRow(i);

      //loop over the colom
      if(idx1 > colCount){
         this.idx1 =0; 
      }
      float x = map( i, 0, table.getRowCount(), u0+border, u0+w-border );
      float y = map( r.getFloat(idx1), minY, maxY, v0+h-(border)-spacer, v0+border+spacer );

      stroke( 0 );
      strokeWeight(1);
      fill(#FF4D4D);
      //noFill();
      rect(x, y, (w)/(table.getRowCount()), h-border-spacer-y);

      if (selected.contains(i)) {
        //fill in rect
        stroke(#0000FF );
        strokeWeight(5);
        rect(x, y, (w)/(table.getRowCount()), h-border-spacer-y);
      } else {

        //fill in rect
      }
    }

    stroke(0);
    strokeWeight(1); 
    noFill();
    rect( u0+border, v0+border, w-2*border, h-2*border);

    if ( drawLabels ) {
      fill(0);
      textAlign(CENTER, TOP);
      text( table.getColumnTitle(idx1), (u0+w/2), (v0+h-spacer)-35 );

      //pushMatrix();
      //translate( u0+10, v0+h/2 );
      //rotate((PI/2));
      //text( table.getColumnTitle(idx1),-50 , -spacer );
      //popMatrix();
    }
  }

  //messes up the mapping but works incorrectly
  void mousePressed() {
    //if (myFrame3.mouseInside()) {
    //  if (this.idx1 == colCount) {
    //    this.idx1 = 0;
    //  } else {
    //    idx1++;
    //    myFrame3.setPosition(0, 0, width/2, height/2);
    //    myFrame3.draw();
    //  }
    //}
  }
}