class LineChart extends Frame {

  float minX, maxX;
  float minY, maxY;
  int idx0, idx1;
  int border = 40;
  boolean drawLabels = true;
  float spacer = 20;
  int colCount;




  LineChart( Table data, ArrayList<Integer> useColumns ) {

    //this.idx0 = idx0;
    //this.idx1 = idx1;
    colCount = useColumns.size();
    for (int i=0; i<colCount; i++) {
      //make an array so that when we click a button the idx0 updates
    }
    idx0 = useColumns.get(0);
    println(idx0);
    idx1 = useColumns.get(0);

    minX = min(data.getFloatColumn(idx0));
    maxX = max(data.getFloatColumn(idx0));

    minY = min(data.getFloatColumn(idx1));
    maxY = max(data.getFloatColumn(idx1));

    //table.getColumnTitle();
    //table.getRowCount()
    //table.getRow()
    // row.getFloat();
  }

  //void updateChart(int idx1) {

  //  if (idx1 <= colCount-1) {
  //    this.idx1 = idx1;
  //  }

  //  println(idx0);


  //  minX = min(table.getFloatColumn(idx0));
  //  maxX = max(table.getFloatColumn(idx0));

  //  minY = min(table.getFloatColumn(idx1));
  //  maxY = max(table.getFloatColumn(idx1));
  //}

  void draw() {

    for ( int i = 0; i < table.getRowCount(); i++ ) {
      TableRow r = table.getRow(i);
      TableRow r2 = table.getRow(i+1);

      float x = map( i, 0, table.getRowCount(), u0+border, u0+w-border );
      float y = map( r.getFloat(idx1), minY, maxY, v0+h-(border)-spacer, v0+border+spacer );

      if (i < table.getRowCount()-1 ) {
        float x1 = map( i+1, 0, table.getRowCount(), u0+border, u0+w-border );
        float y1 = map( r2.getFloat(idx1), minY, maxY, v0+h-(border)-spacer, v0+border+spacer );
        stroke( 0 );
        strokeWeight(1);
        line(x, y, x1, y1);

        if (selected.contains(i)) {
          stroke(#FF4D4D );
          strokeWeight(6);
          line(x, y, x1, y1);
          stroke(#0000FF);
          ellipse(x1, y1, 8, 8);
        }
      }
      stroke( 0 );
      strokeWeight(1);
      fill(#FF4D4D);
      //noFill();
      ellipse(x, y, 4, 4);
      // line(u0+border-10,v0+border+10,u0+border+10,v0+border+10);

      if (selected.contains(i)) {
        //fill in rect
        stroke(#0000FF );
        strokeWeight(5);
        ellipse(x, y, 8, 8);
      } else {
        //fill in rect
      }
    }

    stroke(0);
    strokeWeight(1); 
    noFill();
    //rect( u0+border-10,v0+border+10, w-2*border+15, h-2*border);

    if ( drawLabels ) {
      fill(0);
      textAlign(TOP, CENTER);
      text( table.getColumnTitle(idx1), (u0+w/2)-20, (v0+h-spacer)-20 );

      //pushMatrix();
      //translate( u0+10, v0+h/2 );
      //rotate((PI/2));
      //text( table.getColumnTitle(idx1),-50 , -spacer );
      //popMatrix();
    }
  }

  void mousePressed() {
    if (idx1 > colCount) {
      this.idx1 = 0;
    } else {
      if (myFrame4.mouseInside()) {
        idx1++;
        myFrame4.setPosition(0, height/2, height/2, height/2);
        myFrame4.draw();
      }
    }
  }
}