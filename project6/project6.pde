Frame myFrame = null;
Frame myFrame2 = null;
Frame myFrame3 = null;
Frame myFrame4 = null;
Table table;

void setup() {
  size(1200, 800);
  frameRate(30);
  selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    selectInput("Select a file to process:", "fileSelected");
  } else {
    println("User selected " + selection.getAbsolutePath());
    table = loadTable( selection.getAbsolutePath(), "header" );

    ArrayList<Integer> useColumns = new ArrayList<Integer>();
    for (int i = 0; i < table.getColumnCount(); i++) {
      if ( !Float.isNaN( table.getRow( 0 ).getFloat(i) ) ) {
        println( i + " - type float" );
        useColumns.add(i);
      } else {
        println( i + " - type string" );
      }
    }
    myFrame = new PCP( table, useColumns );
    myFrame2 = new Splom(table, useColumns);
    myFrame3 = new BarChart(table, useColumns);
    myFrame4 = new LineChart(table, useColumns);
  }
}


void draw() {
  background( 255 );

  if ( table == null )
    return;
  
  //PCP
  if ( myFrame != null ) {
    myFrame.setPosition( width/2,0, width/2, height/2 );
    myFrame.draw();
  }
  
  //Scatter & Splom
  if (myFrame2 != null) {
    myFrame2.setPosition(height/2, height/2, height/2, height/2);
    myFrame2.draw();
  }
  
  //BarChart
  if (myFrame3 != null) {
    myFrame3.setPosition(0, 0, width/2, height/2);
    myFrame3.draw();
  }
  
  //LineChart
  if (myFrame4 != null) {
    myFrame4.setPosition(0, height/2, height/2, height/2);
    myFrame4.draw();
  }
}

void mousePressed() {
  myFrame.mousePressed();
  myFrame2.mousePressed();
  myFrame3.mousePressed();
  myFrame4.mousePressed();
}

void mouseReleased() {
  myFrame.mouseReleased();
  myFrame2.mouseReleased();
  myFrame3.mouseReleased();
  myFrame4.mouseReleased();
}

abstract class Frame {

  int u0, v0, w, h;
  int clickBuffer = 2;
  void setPosition( int u0, int v0, int w, int h ) {
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
  }

  abstract void draw();
  void mousePressed() {
  }
  void mouseReleased() {
  }

  boolean mouseInside() {
    return (u0-clickBuffer < mouseX) && (u0+w+clickBuffer)>mouseX && (v0-clickBuffer)< mouseY && (v0+h+clickBuffer)>mouseY;
  }
}