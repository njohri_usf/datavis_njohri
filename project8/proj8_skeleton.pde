
//version 2
Frame myFrame = null;
import java.util.Map;

void setup() {
  size(800, 800);  
  selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    selectInput("Select a file to process:", "fileSelected");
  } else {
    println("User selected " + selection.getAbsolutePath());

    ArrayList<GraphVertex> verts = new ArrayList<GraphVertex>();
    ArrayList<GraphEdge>   edges = new ArrayList<GraphEdge>();


    // TODO: PUT CODE IN TO LOAD THE GRAPH
    JSONObject json = loadJSONObject(selection.getPath());
    JSONArray nodes = json.getJSONArray("nodes");
    JSONArray links = json.getJSONArray("links");
    HashMap<String, GraphVertex> hashmap = new HashMap<String, GraphVertex>();

    //Initailizing all vertex's
    for (int i=0; i<nodes.size(); i++) {
      GraphVertex newVertex = new GraphVertex(nodes.getJSONObject(i).getString("id"), 
        nodes.getJSONObject(i).getInt("group"), 
        random(50, width-50), random(50, height-50));
      verts.add(newVertex);
      hashmap.put(nodes.getJSONObject(i).getString("id"), newVertex);
    }

    //Initalzing all the edges
    for (int i=0; i<links.size(); i++) {
      GraphVertex v0 = hashmap.get(links.getJSONObject(i).getString("source"));
      GraphVertex v1 = hashmap.get(links.getJSONObject(i).getString("target"));
      float value = links.getJSONObject(i).getFloat("value");
      edges.add(new GraphEdge (v0, v1, value));
    }




    myFrame = new ForceDirectedLayout( verts, edges );
  }
}

void addLabels() { //keep the same X value, diff is by 10 for text
  fill(165, 0, 38);
  rect(710, 600, 20, 10);
  fill(0);
  text("Group 0", 740, 610);
  
  fill(215, 48, 39);
  rect(710, 610, 20, 10);
  fill(0);
  text("Group 1", 740, 620);
  
  fill(244, 109, 67);
  rect(710, 620, 20, 10);
  fill(0);
  text("Group 2", 740, 630);
  
  fill(253, 174, 97);
  rect(710, 630, 20, 10);
  fill(0);
  text("Group 3", 740, 640);
  
  fill(254, 224, 144);
  rect(710, 640, 20, 10);
  fill(0);
  text("Group 4", 740, 650);
  
  fill(255, 255, 191);
  rect(710, 650, 20, 10);
  fill(0);
  text("Group 5", 740, 660);

  fill(224, 243, 248);
  rect(710, 660, 20, 10);
  fill(0);
  text("Group 6", 740, 670);

  fill(171, 217, 233);
  rect(710, 670, 20, 10);
  fill(0);
  text("Group 7", 740, 680);

  fill(116, 173, 209);
  rect(710, 680, 20, 10);
  fill(0);
  text("Group 8", 740, 690);

  fill(69, 117, 180);
  rect(710, 690, 20, 10);
  fill(0);
  text("Group 9", 740, 700);

  fill(49, 54, 149);
  rect(710, 700, 20, 10);
  fill(0);
  text("Group 10", 740, 710);
  
  fill(0);
  text("Hover over the vertex to display information", 530, height-30);
}


void draw() {
  background( 255 );

  if ( myFrame != null ) {
    myFrame.setPosition( 0, 0, width, height );
    myFrame.draw();
  }
}

void mousePressed() {
  myFrame.mousePressed();
}

void mouseReleased() {
  myFrame.mouseReleased();
}

//color scheme which is colorblind safe
void colorMap(HashMap<String, Integer> colorMap) {
  colorMap.put("0", color(165, 0, 38));
  colorMap.put("1", color(215, 48, 39));
  colorMap.put("2", color(244, 109, 67));
  colorMap.put("3", color(253, 174, 97));
  colorMap.put("4", color(254, 224, 144));
  colorMap.put("5", color(255, 255, 191));
  colorMap.put("6", color(224, 243, 248));
  colorMap.put("7", color(171, 217, 233));
  colorMap.put("8", color(116, 173, 209));
  colorMap.put("9", color(69, 117, 180));
  colorMap.put("10", color(49, 54, 149));
}

abstract class Frame {

  int u0, v0, w, h;
  int clickBuffer = 2;
  void setPosition( int u0, int v0, int w, int h ) {
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
  }

  abstract void draw();

  void mousePressed() {
  }
  void mouseReleased() {
  }

  boolean mouseInside() {
    return (u0-clickBuffer < mouseX) && (u0+w+clickBuffer)>mouseX && (v0-clickBuffer)< mouseY && (v0+h+clickBuffer)>mouseY;
  }
}