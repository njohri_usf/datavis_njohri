// most modification should occur in this file


class ForceDirectedLayout extends Frame {


  float RESTING_LENGTH = 2.0f;     // These are the values 
  float SPRING_SCALE   = 0.0002f;  // i found to stop my 
  float REPULSE_SCALE  = 2.5f;     // display from exploding
                                   

  float TIME_STEP      = 0.6f;     // probably don't need to update this

  // Storage for the graph
  ArrayList<GraphVertex> verts;
  ArrayList<GraphEdge> edges;
  HashMap<String, Integer> colorMap = new HashMap<String, Integer>(); //colorMapping Hashmap

  // Storage for the node selected using the mouse (you 
  // will need to set this variable and use it) 
  GraphVertex selected = null;


  ForceDirectedLayout( ArrayList<GraphVertex> _verts, ArrayList<GraphEdge> _edges ) {
    verts = _verts;
    edges = _edges;
  }

  void applyRepulsiveForce( GraphVertex v0, GraphVertex v1 ) {
    // CALCULATE (AND APPLY) A REPULSIVE FORCE
    float dist = v0.getPosition().dist(v1.getPosition()); 
    float force_r = ((REPULSE_SCALE)*(v0.getMass()*v1.getMass()))/(pow(dist, 2));

    //distance vector
    PVector PsubQ =  PVector.sub(v0.getPosition(), v1.getPosition());

    //  direction * magnutude
    PVector forceXY = new PVector ((PsubQ.x*force_r), (PsubQ.y*force_r) );

    //adding force to verts. v0 and v1 need to have opposing forces
    v0.addForce(forceXY.x, forceXY.y);
    v1.addForce(-forceXY.x, -forceXY.y);
  }

  void applySpringForce( GraphEdge edge ) {
    // CALCULATE (AND APPLY) A SPRING FORCE
    float dist = edge.v0.getPosition().dist(edge.v1.getPosition()); 
    float max = max(0, dist-RESTING_LENGTH);
    float force_a = max*SPRING_SCALE;

    //distance vector
    PVector QsubP =  PVector.sub(edge.v1.getPosition(), edge.v0.getPosition());

    // magnutude * direction
    PVector forceXY = new PVector ((QsubP.x*force_a), (QsubP.y*force_a) );
    
    //adding force to edges. v0 and v1 need to have opposing forces
    edge.v0.addForce(forceXY.x, forceXY.y);
    edge.v1.addForce(-forceXY.x, -forceXY.y);
  }

  void draw() {
    update(); // don't modify this line/
    addLabels();
    //Inializng colorMaps
    colorMap(colorMap);

    // Drawing the edges
    for (int i=0; i< edges.size(); i++) {
      stroke(0, 0, 0, 50);
      line(edges.get(i).v0.getPosition().x, edges.get(i).v0.getPosition().y, edges.get(i).v1.getPosition().x, edges.get(i).v1.getPosition().y);
    }
    
    //Drawing the vertex
    for (int i=0; i< verts.size(); i++) {
      fill(colorMap.get(verts.get(i).getGroup()));
      verts.get(i).setDiameter(15);
      stroke(2);
      ellipse(verts.get(i).getPosition().x, verts.get(i).getPosition().y, verts.get(i).getDiameter(), verts.get(i).getDiameter());
      
      //mouse interaction which displays the charater name & group 
      if ((mouseX < (verts.get(i).getPosition().x)+8 && mouseX > ((verts.get(i).getPosition().x))-8) 
        && (mouseY < ((verts.get(i).getPosition().y))+8 && mouseY > (verts.get(i).getPosition().y)-8) && 
        (verts.get(i).getAcceleration().x) < 1 && (verts.get(i).getAcceleration().y) < 1 ) {
        fill(0);
        textAlign(LEFT, LEFT);
        text("Character Name: " + verts.get(i).id, 5, height-30); 
        text("Character Group: " + verts.get(i).group, 5, height-15);
        
      }
    }
  }


  void mousePressed() { 
    // TODO: ADD SOME INTERACTION CODE
  }

  void mouseReleased() {    
    // TODO: ADD SOME INTERACTION CODE
  }



  // The following function applies forces to all of the nodes. 
  // This code does not need to be edited to complete this 
  // project (and I recommend against modifying it).
  void update() {
    for ( GraphVertex v : verts ) {
      v.clearForce();
    }

    for ( GraphVertex v0 : verts ) {
      for ( GraphVertex v1 : verts ) {
        if ( v0 != v1 ) applyRepulsiveForce( v0, v1 );
      }
    }

    for ( GraphEdge e : edges ) {
      applySpringForce( e );
    }

    for ( GraphVertex v : verts ) {
      v.updatePosition( TIME_STEP );
    }
  }
}