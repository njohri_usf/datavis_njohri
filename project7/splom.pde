class Splom extends Frame {

  ArrayList<Scatterplot> plots = new ArrayList<Scatterplot>( );
  int colCount;
  Table data;
  float border = 20;
  Scatterplot newPlot;

  Splom( Table data, ArrayList<Integer> useColumns ) {
    this.data = data;
    colCount = useColumns.size();
    newPlot= new Scatterplot(table, useColumns.get(0), useColumns.get(1));
    for ( int j = 0; j < colCount-1; j++ ) {
      for ( int i = j+1; i < colCount; i++ ) {
        Scatterplot sp = new Scatterplot( table, useColumns.get(j), useColumns.get(i) );
        plots.add(sp);
      }
    }


    //table.getColumnCount()
    //table.getColumnType(int column) != Table.STRING
    //table.getColumnTitle();
  }

  void setPosition( int u0, int v0, int w, int h ) {
    super.setPosition(u0, v0, w, h);

    int curPlot = 0;
    for ( int j = 0; j < colCount-1; j++ ) {
      for ( int i = j+1; i < colCount; i++ ) {
        Scatterplot sp = plots.get(curPlot++);
        int su0 = (int)map( i, 1, colCount, u0+border, u0+w-border );
        int sv0 = (int)map( j, 0, colCount-1, v0+border, v0+h-border );
        sp.setPosition( su0, sv0, (int)(w-2*border)/(colCount-1), (int)(h-2*border)/(colCount-1) );
        sp.drawLabels = true;
        sp.border = 3;
      }
    }
    newPlot.setPosition(800, 400, 400, 400);
  }


  void draw() {
    for ( Scatterplot s : plots ) {
      s.draw();
      text("Hover over Parallel Coordinate to highlight all points", 1000,390);
      text("Drag the axis to change Parallel Coordinate", 1000,410);
      text("Click on scatter plot to change views", 600,400);
      newPlot.draw();
    }
  }


  void mousePressed() { 
    for ( Scatterplot sp : plots ) {
      if ( sp.mouseInside() ) {
        newPlot = new Scatterplot (table, sp.idx0, sp.idx1);
        println(sp.idx0 + " " + sp.idx1);
      }
    }
  }
}