import java.util.*;

class BarChart extends Frame {

  Table data;
  int idx0, idx1, idx2, idx3;
  int border = 100;
  boolean drawLabels = true;
  float spacer = 10;
  float floorValue; //floor value post calculation

  //Store the floorValue to map each bin as bargraph
  ArrayList<Integer> GraphBinsVals0 = new ArrayList<Integer>(); //column1
  ArrayList<Integer> GraphBinsVals1 = new ArrayList<Integer>(); //column2
  ArrayList<Integer> GraphBinsVals2 = new ArrayList<Integer>(); //column3
  ArrayList<Integer> GraphBinsVals3 = new ArrayList<Integer>(); //column4
  //ArrayList<Integer> bins4 = new ArrayList<Integer>();

  //min and max of each bin
  ArrayList<Integer> binMin = new ArrayList<Integer>();
  ArrayList<Integer> binMax = new ArrayList<Integer>();



  BarChart( Table data, ArrayList<Integer> useColumns ) {

    this.data = data;
    
    //set index with columns
    idx0 = useColumns.get(0);
    idx1 = useColumns.get(1);
    idx2 = useColumns.get(2);
    idx3 = useColumns.get(3);
    
    //call set bin 4 times for 4 columns
    setBinValues(GraphBinsVals0, idx0, binMax, binMin);
    setBinValues(GraphBinsVals1, idx1, binMax, binMin);
    setBinValues(GraphBinsVals2, idx2, binMax, binMin);
    setBinValues(GraphBinsVals3, idx3, binMax, binMin);
  }
  
  //function to set each cols bins
  void setBinValues(ArrayList<Integer> bin, int index, ArrayList<Integer> max, ArrayList<Integer> min) {
    int bin0=0, bin1=0, bin2=0, bin3=0, bin4=0; //k number of bins
    float minX = min(data.getFloatColumn(index)), maxX = max(data.getFloatColumn(index));
    for ( int i = 0; i < table.getRowCount(); i++ ) {
      TableRow r = table.getRow(i);
      //calucate the floor
      floorValue = floor(4*(r.getFloat(index)-minX)/(maxX-minX));
      switch((int)floorValue) {
      case 0: 
        bin0++;
        break;
      case 1: 
        bin1++;
        break;
      case 2: 
        bin2++;
        break;
      case 3: 
        bin3++;
        break;
      case 4: 
        bin4++;
        break;
      default:
        println("error");
        break;
      }
    }
    bin.add(bin0);
    bin.add(bin1);
    bin.add(bin2);
    bin.add(bin3);
    bin.add(bin4);
    max.add(Collections.max(bin));
    min.add(Collections.min(bin));
  }

  void draw() {

    //index0 - SATM
    for (int i=0; i< GraphBinsVals0.size(); i++) {
      float x = map(i, 0, GraphBinsVals0.size(), u0+100, 300);
      float y = map(GraphBinsVals0.get(i), binMin.get(0), binMax.get(0), v0+h-(100)-spacer, v0+100+spacer);
      stroke( 0 );
      strokeWeight(1);
      fill(#FF4D4D);
      rect(x-50, y+110, 30, h-100-spacer-y);
    }

    //index1 - SATV
    for (int i=0; i< GraphBinsVals1.size(); i++) {
      float x = map(i, 0, GraphBinsVals1.size(), u0+100, 300);
      float y = map(GraphBinsVals1.get(i), binMin.get(1), binMax.get(1), v0+h-(100)-spacer, v0+100+spacer);
      stroke( 0 );
      strokeWeight(1);
      fill(#FF4D4D);
      rect(x-50, y-100, 30, h-100-spacer-y);
    }
    
    //index2 - ACT
    for (int i=0; i< GraphBinsVals2.size(); i++) {
      float x = map(i, 0, GraphBinsVals2.size(), u0+300, u0+w-border);
      float y = map(GraphBinsVals2.get(i), binMin.get(2), binMax.get(2), v0+h-(border)-spacer, v0+border+spacer);
      stroke( 0 );
      strokeWeight(1);
      fill(#FF4D4D);
      rect(x-50, y-100, 30, h-border-spacer-y);
    }
    
    //index3 - GPA
    for (int i=0; i< GraphBinsVals3.size(); i++) {
      float x = map(i, 0, GraphBinsVals3.size(), u0+300, u0+w-border);
      float y = map(GraphBinsVals3.get(i), binMin.get(3), binMax.get(3), v0+h-(border)-spacer, v0+border+spacer);
      stroke( 0 );
      strokeWeight(1);
      fill(#FF4D4D);
      rect(x-50, y+110, 30, h-border-spacer-y);
    }

    stroke(0); 
    strokeWeight(1); 
    noFill(); 
    //rect( u0+40, v0+40, w-2*40, h-2*40); 

    if ( drawLabels ) {
      fill(0); 
      textAlign(CENTER, TOP); 
      text( table.getColumnTitle(idx0), 150, 405); 
      text(table.getColumnTitle(idx1), 150, 200);
      text(table.getColumnTitle(idx2), 350, 200);
      text(table.getColumnTitle(idx3), 350, 405);
      
      //pushMatrix();
      //translate( u0+10, v0+h/2 );
      //rotate((PI/2));
      //text( table.getColumnTitle(idx1),-50 , -spacer );
      //popMatrix();
    }
  }

  //messes up the mapping but works incorrectly
  void mousePressed() {
    //if (myFrame3.mouseInside()) {
    //  if (this.idx1 == colCount) {
    //    this.idx1 = 0;
    //  } else {
    //    idx1++;
    //    myFrame3.setPosition(0, 0, width/2, height/2);
    //    myFrame3.draw();
    //  }
    //}
  }
}