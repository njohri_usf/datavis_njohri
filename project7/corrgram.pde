//I could not figure out why the coloring is not working
//I also didn't have time to finish the PCC


class Corrgram extends Frame {
  int idx0, idx1, idx2, idx3;
  int rowCount = table.getRowCount();
  Table data;
  double corVal;
  //1st set
  ArrayList<Float> MasterListX0 = new ArrayList<Float>(); //holds the data for calcuation X
  ArrayList<Float> MasterListY0 = new ArrayList<Float>(); //holds the data for calcuation Y
  //2nd set
  ArrayList<Float> MasterListX1 = new ArrayList<Float>(); //holds the data for calcuation X
  ArrayList<Float> MasterListY1 = new ArrayList<Float>(); //holds the data for calcuation Y

  //3rd set
  ArrayList<Float> MasterListX2 = new ArrayList<Float>(); //holds the data for calcuation X
  ArrayList<Float> MasterListY2 = new ArrayList<Float>(); //holds the data for calcuation Y

  //4th set
  ArrayList<Float> MasterListX3 = new ArrayList<Float>(); //holds the data for calcuation X
  ArrayList<Float> MasterListY3 = new ArrayList<Float>(); //holds the data for calcuation Y

  //5th set
  ArrayList<Float> MasterListX4 = new ArrayList<Float>(); //holds the data for calcuation X
  ArrayList<Float> MasterListY4 = new ArrayList<Float>(); //holds the data for calcuation Y

  //6th set
  ArrayList<Float> MasterListX5 = new ArrayList<Float>(); //holds the data for calcuation X
  ArrayList<Float> MasterListY5 = new ArrayList<Float>(); //holds the data for calcuation Y

  double meanX, meanY;
  double stdevX, stdevY;
  int hexColor=#F0FFF8;
  
  //CONSTUCTOR
  Corrgram(Table data, ArrayList<Integer> useColumns) {
    this.data = data;
    idx0 = useColumns.get(0); //SATM -
    idx1 = useColumns.get(1); //SATV -
    idx2 = useColumns.get(2); //ACT -
    idx3 = useColumns.get(3); //GPA -
  }
  
  void draw() {
    
    //DRAW EACH RECTANGLE
    noFill();
    setMasterList(idx1, idx0, MasterListX0, MasterListY0 ); //SATV vs SATM
    setMeanAndStdev(MasterListX0, MasterListY0 );
    corVal = Covariance(MasterListX0, MasterListY0);
    text(table.getColumnTitle(idx1),20,500);
    text(table.getColumnTitle(idx0),60,550);
    text((int)corVal,50,560);
    setColor(corVal);
    fill(hexColor);
    drawRect(50, 500);


    noFill();
    setMasterList(idx2, idx0, MasterListX1, MasterListY1 ); //ACT vs SATM
    setMeanAndStdev(MasterListX1, MasterListY1 );
    corVal = Covariance(MasterListX1, MasterListY1);
    text(table.getColumnTitle(idx2),20,600);
    text(table.getColumnTitle(idx0),50,650);
    text((int)corVal,50,660);
    setColor(corVal);
    fill(hexColor);
    drawRect(50, 600);


    noFill();
    setMasterList(idx3, idx0, MasterListX2, MasterListY2 ); //GPA vs SATM
    setMeanAndStdev(MasterListX2, MasterListY2 );
    corVal = Covariance(MasterListX2, MasterListY2);
    setColor(corVal);
    fill(hexColor);
    drawRect(50, 700);


    noFill();
    setMasterList(idx2, idx1, MasterListX3, MasterListY3 ); //ACT vs SATV
    setMeanAndStdev(MasterListX3, MasterListY3 );
    corVal = Covariance(MasterListX3, MasterListY3);
    setColor(corVal);
    fill(hexColor);
    drawRect(150, 600);

    noFill();
    setMasterList(idx3, idx1, MasterListX4, MasterListY4 ); //GPA vs SATV
    setMeanAndStdev(MasterListX4, MasterListY4 );
    corVal = Covariance(MasterListX4, MasterListY4);
    setColor(corVal);
    fill(hexColor);
    drawRect(150, 700);

    noFill();
    setMasterList(idx3, idx2, MasterListX4, MasterListY4 ); //GPA vs ACT
    setMeanAndStdev(MasterListX4, MasterListY4 );
    corVal = Covariance(MasterListX4, MasterListY4);
    setColor(corVal);
    fill(hexColor);
    drawRect(250, 700);
  }
  
  //FINDS THE MEAN AND STDEV
  void setMeanAndStdev(ArrayList<Float> X, ArrayList<Float> Y) {
    meanX = mean(X);
    meanY = mean(Y);
    stdevX = calcuateSD(X);
    stdevY = calcuateSD(Y);
  }
  
  //INITALIZES ALL THE LISTS
  void setMasterList(int index1, int index2, ArrayList<Float> X, ArrayList<Float> Y) {
    for ( int i = 0; i < table.getRowCount(); i++ ) {
      TableRow r = table.getRow(i);
      TableRow r2 = table.getRow(i);
      X.add(r.getFloat(index1));
      Y.add(r2.getFloat(index2));
    }
  }

  //DRAW FUNCTION
  void drawRect(int x, int y) {
    rect((u0+x), y, 50, 50);
  }

  //SETS THE COLOR 
  void setColor(double corVal) {
    if (corVal < 0 && corVal >= -1) {
      hexColor = #1D0AFF;
    } else if (corVal > 0 &&  corVal <= 1) {
      hexColor = #FC0011;
    } else if ( corVal == 0) {
      hexColor = #16F794;
    } else {
      hexColor =0;
    }
  }
  
  //FIND MEAN
  double mean(ArrayList<Float> meanList) {
    double sum = 0;
    for (int i=0; i<meanList.size(); i++) {
      sum = sum + meanList.get(i);
    }
    return sum/meanList.size();
  }
  
  //CALCUATE SD
  double calcuateSD(ArrayList<Float> SD) {
    double sum =0, standardDeviation =0;
    for (double num : SD) {
      sum = sum + num;
    }
    double mean = sum/table.getRowCount();
    for (int i=0; i < table.getRowCount(); i++) {
      standardDeviation = standardDeviation + Math.pow(SD.get(i) - mean, 2);
    }
    return Math.sqrt(standardDeviation)/table.getRowCount();
  }
  
  //GET THE COVARIANCE VALUE
  double Covariance(ArrayList<Float> X, ArrayList<Float> Y) {
    float var= table.getRowCount();
    double CoVar=0;
    double print=0;
    for (int i=0; i < table.getRowCount(); i++) {
      CoVar = (X.get(i)-meanX)*(Y.get(i)-meanY);
      print = CoVar + print;
    }
    print = ((CoVar)*(1/var))/(stdevX*stdevY);
    return (float)print;
  }
}