float[][] rawData;
String[] headers;
String[] data;
Boolean flag = false;
Table t;
int colSize, rowSize;
int margin = 35;
float space;
PVector[] positions;
int idx = 0; //move between graphs

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    data = loadStrings(selection.getPath());
    headers =  split(data[0], ",");

    t = loadTable(selection.getAbsolutePath(), "header");
    rowSize = t.getRowCount();
    colSize = t.getColumnCount();
    rawData = new float[colSize][rowSize];

    int i = 0;
    for (TableRow row : t.rows()) {   
      int j = 0;
      rawData[j][i] = row.getFloat(headers[j++]);
      rawData[j][i] = row.getFloat(headers[j++]);
      rawData[j][i] = row.getFloat(headers[j++]);
      rawData[j][i] = row.getFloat(headers[j++]);
      i++;
    }
    flag = true;
  }
}

void setup() {
  selectInput("Select a file to process:", "fileSelected");
  size(800, 600);
}

void draw() {
  background(255);
  fill(0);
  stroke(255);

  //Line Chart 
  color d = color(156, 117, 27); 
  color blue = color(16, 39, 91);  
  color r = color(91, 16, 39);  

  if (flag == true) {

    positions = new PVector[rowSize];
    space = width / rowSize;
    float maxValues[] = new float[colSize];
    float minValues[] = new float[colSize];
    for (int x = 0; x < colSize; x++) {
      maxValues[x] = max(rawData[x]);
      minValues[x] = min(rawData[x]);
    }

    for (int y=0; y < rowSize; y++) {
      float nScore = map(rawData[idx][y], minValues[idx], maxValues[idx], 0, height - (3*margin));
      float ycor = height - (margin) - nScore - 40;
      float xcor = margin + (space * 1.35*y);
      positions[y] = new PVector(xcor, ycor);
    }

    for (int h = 0; h < rowSize; h++) {
      fill(0);  
      stroke(0);
      if (h>0) {
        //connecting lines
        line(positions[h].x, positions[h].y, positions[h-1].x, positions[h-1].y);
      }
      ellipse (positions[h].x, height - margin, 1, 1);
      stroke(d);
      fill(r); 
      ellipse(positions[h].x, positions[h].y, 6, 6);
    }   

    if (mouseX > margin && mouseX < width - (margin*2) - 1 ) {
      float location = mouseX - margin;
      int i = round(location*0.39);
      text(round(rawData[idx][i]), positions[i].x, positions[i].y - 10); 
      text("Details: ", margin, margin);
      fill(0);
      int h = margin + 15;
      for (int o = 0; o < colSize; o++) {
        text(headers[o] + ": " + rawData[idx][i], 65, h);
        h += 15;
      }
      
      //lines box
      fill(blue);
      //vertical Lines
      line(margin - 5, margin-15, margin - 5, h - 10);
      line(180, margin - 15, 180, h - 10);
      //horizontal Lines
      line(180, margin-15, margin - 5, margin-15);
      line(margin - 5, h - 10, 180, h - 10);
    }

    //Legends 
    stroke(255);
    fill(0);
    text(headers[idx] + " Line Graph", width/2 - 50, 34);
    fill(blue);
  }
}

//function to move next map
void mouseClicked() {
  if (idx == colSize - 1) {
    idx = 0;
  }
  idx++;
}