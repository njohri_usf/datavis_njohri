float[][] rawData;
String[] headers;
String[] data;
int idx = 0; //move between graphs
Boolean flag = false;
float multiplier;
Table t;
int colSize, rowSize;

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    data = loadStrings(selection.getPath()); 
    headers =  split(data[0], ",");

    t = loadTable(selection.getAbsolutePath(), "header");
    rowSize = t.getRowCount();
    colSize = t.getColumnCount();
    rawData = new float[colSize][rowSize];
    int i = 0;
    for (TableRow row : t.rows()) {   
      int j = 0;
      rawData[j][i] = row.getFloat(headers[j++]);
      rawData[j][i] = row.getFloat(headers[j++]);
      rawData[j][i] = row.getFloat(headers[j++]);
      rawData[j][i] = row.getFloat(headers[j++]);
      i++;
    }
    flag = true;
  }
}

void setup() {
  selectInput("Select a file to process:", "fileSelected");
  size(800, 600);
}

void draw() {
  background(255);
  fill(0);
  stroke(255);

  color blue = color(16, 39, 91);  
  color red = color(0, 122, 0);  
  //Bar Chart 
  if (flag == true) {
    text("Mouse Click, to change graphs", 300, 50*2);
    text("Move the mouse through the bars to see more information", 250, 60*2+15);

    fill(red);
    if (idx == 2) { //
      multiplier = 7.2;
      fill(183, 183, 76);
    } else
      if (idx == 3) {
        multiplier = 65;
        fill(204, 0, 0);
      } else { 
        if(headers[idx] == "Sepal Length" ||headers[idx] == "Sepal Width" ){
          multiplier = 8;
        }
        multiplier = 0.3;
      }
    //text info

    rect(width-100, 240, 15, 15);

    for (int i = 0; i < rowSize; i++) {
      float rectWidth = ((width) / rowSize)*1.295;
      float ypos = height - (rawData[idx][i] * multiplier)-50;
      rect((rectWidth*i)+50, ypos, rectWidth, (rawData[idx][i]*multiplier));
    }
    fill(0);
    textSize(14);

    //legends 
    fill(0);
    text(headers[idx], width-80, 254);
    text(headers[idx] + " Bar Graph", (width/2)-80, 254);
    stroke(0);
    line(50, height - 310, 50, height - 50);
    textSize(10);
    if (idx == 0 || idx == 1 || idx == 2)
      text(round(max(rawData[idx])) +" -", 25, height - 305);
    else
      text(max(rawData[idx]) +" -", 25, height - 305);
    textSize(14);
    //Maouse over a bar 
    if (mouseX > 50 && mouseX < width - 75 && mouseY > height - 310 && mouseY < height - 50) {
      fill(blue);
      stroke(blue);
      float location = mouseX - 50;
      float rectWidth = ((width) / rowSize)*1.2999;
      int i = round(location*((width - 100)/ rowSize)/5);
      float ypos = height - (rawData[idx][i] * multiplier)-50;
      rect((rectWidth*i)+50, ypos, rectWidth, (rawData[idx][i]*multiplier));

      text("Information:", 55, 204);
      int space = 219;
      fill(0);
      for (int o = 0; o < colSize; o++) {
        text(headers[o] + ": " + rawData[idx][i], 65, space);
        space += 15;
      }

      //lines box
      fill(blue);

      //vertical Lines
      line(40, 190, 40, space - 10);
      line(180, 190, 180, space - 10);

      //horizontal Lines
      line(180, 190, 40, 190);
      line(40, space - 10, 180, space - 10);
      
    }
  }
}

//function to move next map
void mouseClicked() {
  if (idx == colSize - 1) {
    idx = 0;
  }
  idx++;
}