float[][] rawData;
String[] headers;
String[] data;
Boolean flag = false;
Table t;
int colSize, rowSize;
int margin = 35;
float space;

PVector[] positions;
int coordinates = 0;
float maxValues[];

int idx = 0;

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    data = loadStrings(selection.getPath()); // put all data into CSVdata split by rows
    headers =  split(data[0], ",");

    t = loadTable(selection.getAbsolutePath(), "header");
    rowSize = t.getRowCount();
    colSize = t.getColumnCount();
    rawData = new float[colSize][rowSize];
    // --- Storing data
    int i = 0;
    for (TableRow row : t.rows()) {   
      int j = 0;
      rawData[j][i] = row.getFloat(headers[j++]);
      rawData[j][i] = row.getFloat(headers[j++]);
      rawData[j][i] = row.getFloat(headers[j++]);
      rawData[j][i] = row.getFloat(headers[j++]);
      i++;
    }
    flag = true;

    positions = new PVector[rowSize];
    space = width / rowSize;

    maxValues= new float[colSize];
    float minValues[] = new float[colSize];
    for (int x = 0; x < colSize; x++) {
      maxValues[x] = max(rawData[x]);
      minValues[x] = min(rawData[x]);
    }

    idx = 0; // x-axis
    int idx2 = 1; // y-axis

    for (int y=0; y < rowSize; y++) {
      float loc_x = map(rawData[idx][y], 200, maxValues[idx], 0, width - (2*margin));
      float loc_y = map(rawData[idx2][y], 200, maxValues[idx2], 0, height - (2*margin));
      float ycor = height - margin - loc_y;
      float xcor =  loc_x + margin;
      positions[y] = new PVector(xcor, ycor);
    }
  }
}

void setup() {
  selectInput("Select a file to process:", "fileSelected");
  size(800, 600);
}

void draw() {
  background(255);
  fill(0);
  stroke(255);

  color p = color(51, 153, 51); //dark pinnk color 
  for (int h = 0; h < rowSize; h++) {
    fill(p);
    ellipse(positions[h].x, positions[h].y, 9, 9);
  }
  //horizontal numbers 
  int num = 200;
  int scal = margin;
  while (num <= 800) {
    fill(0);
    text(num, scal - 10, height - margin + 19);
    ellipse(scal, height - margin, 4, 4);
    scal = scal + (61);
    num = num + 50;
  }
  //Vertical numbers 
  int number = 200;
  float scale = height - margin+3;
  while (number <= 800) {
    fill(0);
    text(number + " -", margin -27, scale);
    scale = scale - (45);
    number = number + 50;
  }
  stroke(0);
  //vertical line
  line(margin, margin - 10, margin, height - margin);
  //horizontal lines 
  line(margin, height - margin, width - margin, height - margin);


  textSize(10);
  color r = color(91, 16, 39);  // Color Pantoned Solid Red

  if (mouseX > margin && mouseX < width- margin) {
    for (int t = 0; t < rowSize; t++) {
      if (mouseX >= positions[t].x -5 && mouseX <= positions[t].x + 5 
      && mouseY >= positions[t].y -5 && mouseY <= positions[t].y +5) {
        fill(183, 183, 76);
        text("Details: ", margin +30, margin);
        fill(0);
        int h = margin + 15;
        stroke(0);
        for (int o = 0; o < colSize; o++) {
          text(headers[o] + ": " + rawData[idx][t], 95, h);
          h += 15;
        }
        fill(183, 183, 76);
        ellipse(positions[t].x, positions[t].y, 9, 9);

        //vertical Lines
        line(margin +20, margin-15, margin +20, h - 10);
        line(180, margin - 15, 180, h - 10);

        //horizontal Lines
        line(180, margin-15, margin +20, margin-15);
        line(margin +20, h - 10, 180, h - 10);
        stroke(183, 183, 76);
      }
    }
  }

}

//function to move next map
void mouseClicked() {
  if (idx == colSize - 1) {
    idx = 0;
  }
  idx++;
}