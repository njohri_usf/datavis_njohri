// Project 3
// Sketch1 Create scatterlot of SAT math(xcord) vs SAT verbal(ycord) 

//globals
float[][] newTable;
String[] headers;
String[] rawData;
Table table;
int colSize, rowSize, margin=35,count=0;
float space,graphHeight;
float[] maxVals;
PVector[] mapPosition;
Boolean flag = false;
int index;

/*
    Function to read in file. A flag is set to true once file is read and data is
    loaded into appropriate arrays.
*/
void fileSelected(File selection){
  //User has opend a file otherwise throw error
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    
    //read in file, seperate header, get row & col size
    rawData = loadStrings(selection.getAbsolutePath());
    headers = split(rawData[0], ",");
    table = loadTable(selection.getAbsolutePath(), "header");
    rowSize = table.getRowCount();
    colSize = table.getColumnCount();
    
    //Create new arrays to hold SATM  vs SATV data
    newTable = new float[colSize][rowSize];
    int i=0;
    for(TableRow row : table.rows()){
     int j=0;
     newTable[j][i] = row.getInt(headers[j++]); //SATM
     newTable[j][i] = row.getInt(headers[j++]); //SATV
     newTable[j][i] = row.getInt(headers[j++]);
     newTable[j][i] = row.getFloat(headers[j++]);
     i++;
    }
    //set flag to true indicating we've read data correclty
    flag = true;
    
    //finding graph limits and max(x,y)
    mapPosition = new PVector[rowSize];
    space = width / rowSize;
    graphHeight = (height - (2*margin))/rowSize;
    maxVals = new float[colSize];
    float minVals[] = new float[colSize];
    for(int x=0; x<colSize; x++){
     maxVals[x] = max(newTable[x]);
     minVals[x] = min(newTable[x]);
    }
    
    index = 2;
    int index2 = 3;
    //creat a map for locations
    for(int y=0; y< rowSize; y++){
      float locX = map(newTable[index][y], 0, maxVals[index], 0, width - (margin*4));
      float locY = map(newTable[index2][y], 0, maxVals[index2], 0, height - (margin*4));
      float yCoord = height - margin - locY;
      float xCoord = locX + margin;
      mapPosition[y] = new PVector(xCoord, yCoord);
      println(mapPosition[y]);
    }
  }
}

void setup(){
  size(800,600);
  selectInput("Select a file to process:", "fileSelected");
}



void draw(){
  background(#FFFFFF);
  fill(0);
  stroke(255);
  color blue = color(16,39,91); //blue
  
  //file correclty passed and data read
  if(flag == true){
    for(int h=0; h<rowSize; h++){
     fill(blue);
     ellipse(mapPosition[h].x, mapPosition[h].y, 9,9); 
    }
    
    //draw x-axis
    int xAxis = 0;
    int scale = margin-10;
    while(xAxis <= 36){
      fill(0);
      text(xAxis, scale, height-margin+19);
      ellipse(scale+10, height- margin, 4, 4);
      scale = scale + 39;
      xAxis = xAxis + 2; 
    }
    
    //draw y-axis
    float yAxis = 0;
    float newScale = height - margin+3;
    while(yAxis <= 4){
     fill(0);
     text(yAxis+ " -", margin-26, newScale);
     newScale = newScale - (58);
     yAxis = yAxis + 0.5;
    }
    
     stroke(0);
    //draw horizontal line
    line(margin, height - margin, width - margin + 10 , height - margin);
    
    //draw vertical line
    line(margin, margin - 10, margin , height - margin);
    text("ACT vs GPA", width/2 - 25, margin);
    textSize(10);
    text("ACT", width/2 - 20, height -50);
    text("G", newScale + 10, height/2 -13);
    text("P", newScale + 10 , height/2 );
    text("A", newScale + 10 , height/2 +13);
  } 
} 