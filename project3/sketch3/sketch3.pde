// Project 3
// Sketch1 Create scatterlot of SAT math(xcord) vs SAT verbal(ycord) 

//globals
float[][] newTable;
String[] headers;
String[] rawData;
Table table;
int colSize, rowSize, margin=35,count=0;
float space,graphHeight;
float[] maxVals;
PVector[] mapPosition;
Boolean flag = false;
int index;

/*
    Function to read in file. A flag is set to true once file is read and data is
    loaded into appropriate arrays.
*/
void fileSelected(File selection){
  //User has opend a file otherwise throw error
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    
    //read in file, seperate header, get row & col size
    rawData = loadStrings(selection.getAbsolutePath());
    headers = split(rawData[0], ",");
    table = loadTable(selection.getAbsolutePath(), "header");
    rowSize = table.getRowCount();
    colSize = table.getColumnCount();
    
    //Create new arrays to hold SATM  vs SATV data
    newTable = new float[colSize][rowSize];
    int i=0;
    for(TableRow row : table.rows()){
     int j=0;
     newTable[j][i] = row.getInt(headers[j++]); //SATM
     newTable[j][i] = row.getInt(headers[j++]); //SATV
     newTable[j][i] = row.getInt(headers[j++]);
     newTable[j][i] = row.getFloat(headers[j++]);
     i++;
    }
    //set flag to true indicating we've read data correclty
    flag = true;
    
    //finding graph limits and max(x,y)
    mapPosition = new PVector[rowSize*colSize*4];
    space = width / colSize;
    graphHeight = (height - (2*margin))/colSize;
    maxVals = new float[colSize];
    float minVals[] = new float[colSize];
    for(int x=0; x<colSize; x++){
     maxVals[x] = max(newTable[x]);
     minVals[x] = min(newTable[x]);
    }
    
    index = 2;
    int index2 = 3;
    
    for(int idx =0; idx < colSize; idx++){
     for(int count =0; count < colSize; count++){
       for(int j=0; j< table.getRowCount(); j++){
         if(idx !=count && idx < count){
          float loc_x = map(newTable[idx][j], 0, maxVals[idx], idx * graphHeight, (idx+1)*graphHeight);
          float loc_y = map(newTable[count][j], 0, maxVals[count], count * graphHeight,(count+1)*graphHeight);
          float ycor = height - margin - loc_y + 5;
          float xcor =  loc_x + margin -20 ;
          mapPosition[index++] = new PVector(xcor, ycor);   
         }
       }
     }
    }
  }
}

void setup(){
  size(800,600);
  selectInput("Select a file to process:", "fileSelected");
}



void draw(){
  background(#FFFFFF);
  fill(0);
  stroke(255);
  color blue = color(16,39,91); //blue
  
  //file correclty passed and data read
  if(flag == true){
    color b = color(16,39,91);  // Color Blue
    color r = color(0,122,0);  // Color Pantoned Solid Red
    
    float scr = graphHeight + margin;
    line(margin, margin, margin, height - margin);
    line(margin, margin, width -  margin, margin);
    
    for(int x = 0; x < colSize ; x++){
    line(scr, margin, scr, height - margin );
    line(margin,  scr, width - margin, scr );

    scr += graphHeight;
    }
    
    color p = color(224,0,112); //blue  color 
    for(int h = 0; h < index; h++){
      fill(p);
      ellipse(mapPosition[h].x, mapPosition[h].y,4,4);
    } 
    
     fill(0);
    text("SATM", 10+margin*2, margin-5);
    text("SATV", graphHeight +80, margin-5);
    text("ACT", graphHeight*3-40, margin-5);
    text("GPA", graphHeight*4-40, margin-5);
    text("G", margin - 15, 90);
    text("P", margin - 15 , 103);
    text("A", margin - 15 , 115);
    text("A", margin - 15, 230);
    text("C", margin - 15 , 243);
    text("T", margin - 15 , 256);
    text("S", margin - 15 , 344);
    text("A", margin - 15, 357);
    text("T", margin - 15 , 370);
    text("V", margin - 15 , 383);
    text("S", margin - 15 , 484);
    text("A", margin - 15, 497);
    text("T", margin - 15 , 510);
    text("M", margin - 15 , 523);
  } 
} 