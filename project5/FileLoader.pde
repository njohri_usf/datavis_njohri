import java.util.*;
Frame myFrame = null;
Table table;
float maxValX, minValX;


void setup() {
  size(800, 400);  
  selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    selectInput("Select a file to process:", "fileSelected");
  } else {
    println("User selected " + selection.getAbsolutePath());
    table = loadTable( selection.getAbsolutePath(), "header" );

    ArrayList<Integer> useColumns = new ArrayList<Integer>();
    for (int i = 0; i < table.getColumnCount(); i++) {
      if ( !Float.isNaN( table.getRow( 0 ).getFloat(i) ) ) {
        println( i + " - type float" );
        useColumns.add(i);
      } else {
        println( i + " - type string" );
      }
    }

    //print the table
    printTable();

    myFrame = new ParallelCord( table, useColumns );
  }
}

void printTable() {
  for (int i=0; i<table.getRowCount(); i++) {
    println();
    for (int j=0; j<table.getColumnCount(); j++) {
      print("\t\t"+table.getString(i, j));
    }
  }
}

void draw() {
  background( 255 );

  if ( table == null ) 
    return;

  if ( myFrame != null ) {
    myFrame.setPosition( 0, 0, width, height );
    myFrame.draw();
  }
}

abstract class Frame {

  int u0, v0, w, h;
  //int clickBuffer = 2;
  void setPosition( int u0, int v0, int w, int h ) {
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
  }

  abstract void draw();
}