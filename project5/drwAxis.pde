class drwAxis extends Frame {
  float minX, maxX;
  float minY, maxY;
  float leftAxis =100;
  float topSpace = 20;

  float leftSpace = 50;
  float bottomSpace = 50;
  Table data;

  drwAxis(Table data, int idx, int idx2) {
    this.data = data;
    minX = min(data.getFloatColumn(idx));
    maxX = max(data.getFloatColumn(idx));

    //minY = min(data.getFloatColumn(idx));
    //maxY = max(data.getFloatColumn(idx));
  }

  void draw() {
    stroke(0);
    //strokeWeight(1);
    line(leftAxis, height-bottomSpace, leftAxis, topSpace); 
    drawChart();
  }

  void drawChart() {
    beginShape();
    for (int row=0; row < data.getRowCount(); row++) {
      for (int col=0; col < data.getColumnCount(); col++) {
        TableRow r = data.getRow(row);
        float x = leftSpace;
        float y = map(r.getFloat(col), minX, maxX, height-leftSpace-leftAxis, leftSpace+leftSpace);
        vertex(x+(col*leftSpace), y);
      }
    }
    endShape();
  }
}