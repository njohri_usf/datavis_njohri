class ParallelCord extends Frame {

  ArrayList<drwAxis> Axis = new ArrayList<drwAxis>();
  int colCount;
  Table data;

  ParallelCord(Table data, ArrayList<Integer> useColumns) {
    this.data = data;
    colCount = useColumns.size();
    for (int j = 0; j < colCount-1; j++) {
      for ( int i = j+1; i < colCount; i++ ) {
        drwAxis dw = new drwAxis(table, useColumns.get(j), useColumns.get(i));
        Axis.add(dw);
      }
    }
  }
  //do not use this 
  void setPosition( int u0, int v0, int w, int h ) {
    super.setPosition(u0, v0, w, h);
    
    int curPlot = 0;
    for ( int j = 0; j < colCount-1; j++ ) {
      for ( int i = j+1; i < colCount; i++ ) {
        drwAxis dw = Axis.get(curPlot++);

        
      }
    }
  }

  //Draw each and everything here
  void draw() {
    for (drwAxis s : Axis ) {
      s.draw();
    }
  }
}